Deface::Override.new(:virtual_path => "spree/shared/_header", 
                     :name => "example-1", 
                     :replace => "div#spree-header", 
                     :text => '<header id="header">
  <div class="head-top">
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-12 shopping-links">
          <span class="shipping">
            <i class="fa fa-truck"></i>&nbsp;
            Free Shipping over $50
          </span>

          <span class="shipping hidden-xs">
            <i class="fa fa-ban" style="padding-right:2px;"></i> <a href="#">No Tax*</a>
          </span>

          <span class="price-match">
            <i class="fa fa-tags"></i>&nbsp;
            <a href="#">We price match!</a>
          </span>
        </div>

        <div class="col-md-6 col-sm-6 col-xs-12 account-links">
          <span class="phone">
            <i class="fa fa-phone"></i>
						<span>
            	888-411-5282
						</span>
          </span>

          <span>
            <i class="fa fa-user"></i>&nbsp;
              <a class="text" href="/login">Sign In</a>
              &nbsp;| &nbsp;<a href="/signup">Create Account</a>
          </span>

      <ul class="shopping-cart" data-hook>
      <li id="link-to-cart" data-hook>
        <noscript>
          <%= link_to Spree.t(:cart), "/cart" %>
        </noscript>
      </li>
      <script>Spree.fetch_cart()</script>
    </ul>
        </div>

      </div>
    </div>
  </div>

    <div class="container">
      <div class="row">
        <div class="col-md-8 hidden-sm hidden-xs column">
          <a href="#"><img alt="Whole Latte Love | Everything Coffee" class="logo" src="./Espresso Machines, Coffee Makers &amp; Coffee - WholeLatteLove.com_files/wll-logo-4d81070b66511a17ac16fc63672a4083.png"></a>

          <nav id="header-subnav">
            <ul>
              <li>
                <a href="#">
                  <div class="line1" style="color:#cb1c1c"><i class="fa fa-tag" style="font-size:1em;"></i>Sale</div>
                  <div class="line2" style="color:#cb1c1c">Great deals.</div>
</a>              </li>

              <li>
                <a href="#" target="_blank">
                  <div class="line1"><i class="fa fa-envelope-o"></i><span>Subscribe</span></div>
                  <div class="line2">For sales &amp; more!</div>
                </a>
              </li>

              <li class="hidden-md">
                <a href="#">
                  <div class="line1"><i class="fa fa-video-camera"></i><span>Videos</span></div>
                  <div class="line2">Review &amp; compare.</div>
</a>              </li>

              <li style="margin-right: 0">
                <a class="compare-link disabled" href="#">
                  </a><div class="line1"><a class="compare-link disabled" href="#">
                    <i class="fa fa-balance-scale"></i>Compare
                    <div class="compare-count">0</div>

</a>    
                  </div>

                  <div class="line2">Up to 6 items.</div>
              </li>
            </ul>
          </nav>
        </div>

        <div class="col-md-4 col-sm-12 column header-right">
          <div class="search-area">
  <div class="search-bar">
    <div class="site-selector">
      <div class="facade">
        <span class="selected">Store</span>
        <i class="fa fa-caret-down"></i>
      </div>
    </div>
    
    <div class="site-search">
      <div class="search-store search-input">
        <form accept-charset="UTF-8" action="#" class="search-form" id="searchform" method="get">
          <input name="utf8" type="hidden" value="✓">
          <div class="searchbar">
            <input class="form-control input-sm" id="search" name="keywords" placeholder="Search Products on Our Store" type="search" style="padding-left: 80px;">
            <button class="search-submit disabled" disabled="disabled" type="submit"><i class="fa fa-search"></i></button>
          </div>
        </form>
      </div>
      
      <div class="search-community search-input">
        <form class="navbar-search" action="#" method="get">
          <input type="text" name="tq" class="form-control input-sm" id="community-search" placeholder="Search The Community" autocomplete="off">
          <button class="search-submit disabled" disabled="disabled" type="submit"><i class="fa fa-search"></i></button>
        </form>
      </div>
    </div>
  </div>
  
  <ul class="site-list">
    <li data-site="Store">Store</li>
    <li data-site="Community">Community</li>
  </ul>
</div>

        </div>
      </div>
    </div>

    <div class="container navbar-outer">
  <div class="navbar-wrapper affix-top" data-spy="affix" data-offset-top="100">
    <div class="container navbar hidden-sm hidden-xs">
      <div class="col-md-12">
        <div class="right-link">
          <a href="#">Community</a>
        </div>

        <ul class="nav">
          <li class="logobug"><a href="#"><img alt="Whole Latte Love | Everything Coffee" src="./Espresso Machines, Coffee Makers &amp; Coffee - WholeLatteLove.com_files/wll-logobug-ef077ea13f9111392fe952b17296b4fd.png"></a></li>

              <li class="taxon">
                <a href="#">Espresso Machines</a>
                  <ul class="nav-dropdown row">
                      	<li>
		<a href="#">
			Semi-Automatic
</a>	</li>

                      	<li>
		<a href="#">
			Super-Automatic
</a>	</li>

                      	<li>
		<a href="#">
			Prosumer
</a>	</li>

                      	<li>
		<a href="#">
			Manual
</a>	</li>

                      	<li>
		<a href="#">
			Single-Serve
</a>	</li>

                      	<li>
		<a href="#">
			Machine Packages
</a>	</li>

                      	<li>
		<a href="#">
			Refurbished Machines
</a>	</li>

                  </ul>
            </li>
              <li class="taxon">
                <a href="#">Coffee Makers</a>
                  <ul class="nav-dropdown row">
                      	<li>
		<a href="#">
			Drip Brewers
</a>	</li>

                      	<li>
		<a href="#">
			French Press
</a>	</li>

                      	<li>
		<a href="#">
			Pour-Over
</a>	</li>

                      	<li>
		<a href="#">
			Vacuum Siphon
</a>	</li>

                      	<li>
		<a href="#">
			Single-Serve
</a>	</li>

                      	<li>
		<a href="#">
			Stove Top Coffee
</a>	</li>

                      	<li>
		<a href="#">
			Coldbrew
</a>	</li>

                      	<li>
		<a href="#">
			Non-Electric Brewers
</a>	</li>

                  </ul>
            </li>
              <li class="taxon">
                <a href="#">Coffee &amp; Espresso</a>
                  <ul class="nav-dropdown row">
                      	<li>
		<a href="#">
			Whole Bean Coffee
</a>	</li>

                      	<li>
		<a href="#">
			Whole Bean Decaf Coffee
</a>	</li>

                      	<li>
		<a href="#">
			Pre-Ground Espresso
</a>	</li>

                      	<li>
		<a href="#">
			Micro Roasted Coffees
</a>	</li>

                      	<li>
		<a href="#">
			Coffee Pods and Capsules
</a>	</li>

                      	<li>
		<a href="#">
			Pre-Ground Drip Coffee
</a>	</li>

                      	<li>
		<a href="#">
			Keurig K-Cup Packs
</a>	</li>

                      	<li>
		<a href="#">
			All Decaf Coffee
</a>	</li>

                      	<li>
		<a href="#">
			Coffee Packages
</a>	</li>

                  </ul>
            </li>
              <li class="taxon">
                <a href="#">Grinders</a>
                  <ul class="nav-dropdown row">
                      	<li>
		<a href="#">
			Burr
</a>	</li>

                      	<li>
		<a href="#">
			Blade
</a>	</li>

                      	<li>
		<a href="#">
			Dosing Grinders
</a>	</li>

                      	<li>
		<a href="#">
			Manual Coffee Grinders
</a>	</li>

                      	<li>
		<a href="#">
			Refurbished Grinders
</a>	</li>

                  </ul>
            </li>
              <li class="taxon">
                <a href="#">Accessories</a>
                  <ul class="nav-dropdown row">
                      	<li>
		<a href="#">
			Cups and Saucers
</a>	</li>

                      	<li>
		<a href="#">
			Tampers
</a>	</li>

                      	<li>
		<a href="#">
			Frothing Pitchers
</a>	</li>

                      	<li>
		<a href="#">
			Knock Boxes
</a>	</li>

                      	<li>
		<a href="#">
			Storage
</a>	</li>

                      	<li>
		<a href="#">
			Equipment Extras
</a>	</li>

                      	<li>
		<a href="#">
			Kitchen Tools
</a>	</li>

                      	<li>
		<a href="#">
			Learning Tools
</a>	</li>

                      	<li>
		<a href="#">
			Syrups and Sauces
</a>	</li>

                      	<li>
		<a href="#">
			Espresso Machine Bases
</a>	</li>

                      	<li>
		<a href="#">
			Clothing and Merchandise
</a>	</li>

                  </ul>
            </li>
              <li class="taxon">
                <a href="#">Tea</a>
                  <ul class="nav-dropdown row">
                      	<li>
		<a href="#">
			Teas
</a>	</li>

                      	<li>
		<a href="#">
			Tea Capsules and Cartridges
</a>	</li>

                      	<li>
		<a href="#">
			Tea Brewers
</a>	</li>

                      	<li>
		<a href="#">
			Hot Water Kettles
</a>	</li>

                      	<li>
		<a href="#">
			Tea Gift Sets
</a>	</li>

                  </ul>
            </li>
              <li class="taxon">
                <a href="#">Parts / Care</a>
                  <ul class="nav-dropdown row">
                      	<li>
		<a href="#">
			Cleaning Products
</a>	</li>

                      	<li>
		<a href="#">
			Water Filters
</a>	</li>

                      	<li>
		<a href="#">
			Coffee Filters
</a>	</li>

                      	<li>
		<a href="#">
			Group Gaskets
</a>	</li>

                      	<li>
		<a href="#">
			Portafilters and Filter Baskets
</a>	</li>

                      	<li>
		<a href="#">
			Frothing &amp; Steaming
</a>	</li>

                      	<li>
		<a href="#">
			Spare Glass and Thermal Carafes
</a>	</li>

                      	<li>
		<a href="#">
			All Other Parts
</a>	</li>

                  </ul>
            </li>
              <li class="taxon">
                <a href="#">Commercial</a>
                  <ul class="nav-dropdown row">
                      	<li>
		<a href="#">
			Commercial Espresso Machines
</a>	</li>

                      	<li>
		<a href="#">
			Commercial Grinders
</a>	</li>

                      	<li>
		<a href="#">
			Commercial Coffee Brewers
</a>	</li>

                      	<li>
		<a href="#">
			Commercial Tea Brewers
</a>	</li>

                      	<li>
		<a href="#">
			Commercial Accessories
</a>	</li>

                      	<li>
		<a href="#">
			Commercial Packages
</a>	</li>

                      	<li>
		<a href="#">
			Service and Install Programs
</a>	</li>

                      	<li>
		<a href="#">
			Vending Machines
</a>	</li>

                  </ul>
            </li>
          <li class="taxon"><a href="#">Brands</a></li>
        </ul>
      </div>
    </div>
  </div>
</div>

</header>')