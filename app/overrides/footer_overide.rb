Deface::Override.new(:virtual_path => 'spree/layouts/spree_application',
                     :name => 'fontawsomeLink',
                     :insert_after => 'div.container',
                      :text => '</header>
<footer id="footer">
  <div class="container why-wll">
    <div class="row">
      <div class="col-md-3 col-sm-6 col-xs-12 column">
        <span class="circle" data-elevio-article="32824">
          <div class="icon">
            <span class="fa fa-truck"></span><span class="height"></span>
          </div>
          <div class="title">
            Free Shipping
          </div>
          <div class="text">
            on all orders over $50
          </div>
        </span>
      </div>

      <div class="col-md-3 col-sm-6 col-xs-12 column">
        <a href="https://www.wholelattelove.com/pages/price-match-form">
          <div class="icon">
            <span class="fa fa-tags"></span><span class="height"></span>
          </div>

          <div class="title">
            We Price Match
          </div>

          <div class="text">
            Always get the best price.
          </div>
        </a>
      </div>

      <div class="col-md-3 col-sm-6 col-xs-12 column">
        <a href="https://www.wholelattelove.com/pages/latte-rewards">
          <div class="icon">
            <span class="icon-angel-bean"></span>
            <span class="height"></span>
          </div>

          <div class="title">
            Latte Rewards
          </div>

          <div class="text">
            points earned with every purchase
          </div>
        </a>
      </div>

      <div class="col-md-3 col-sm-6 col-xs-12 column">
        <a href="https://wiki.wholelattelove.com/Main_Page">
          <div class="icon">
            <span class="icon-latte-cup"></span><span class="height"></span>
          </div>

          <div class="title">Support Library</div>

          <div class="text">product info and<br>maintenance wiki</div>
        </a>
      </div>
    </div>
  </div>

  <div class="email-signup">
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-sm-6 col-xs-12 column cta">
        SIGNUP FOR EXCLUSIVE DEALS, INFO AND MORE!
      </div>

      <div class="col-md-6 col-sm-6 col-xs-12 column mailchimp">
        <form action="https://wholelattelove.us9.list-manage.com/subscribe/post?u=6fe3a7db607a2e2776776cab4&amp;id=6263d3b05c" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate form-inline signup-form" target="_blank" novalidate="">
            <fieldset>
              <input type="text" name="EMAIL" placeholder="Enter your email address."><input type="submit" value="+">
            </fieldset>
            <div id="mce-responses" class="clear">
              <div class="response" id="mce-error-response" style="display:none"></div>
              <div class="response" id="mce-success-response" style="display:none"></div>
            </div>
            <div style="position: absolute; left: -5000px;"><input type="text" name="b_6fe3a7db607a2e2776776cab4_6263d3b05c" tabindex="-1" value=""></div>
        </form>
      </div>
    </div>
  </div>
</div>


  <div class="row sitemap">
    <div class="container">
      <div class="col-md-9">
        <div class="sitemap-title">Whole Latte Love</div>
          <div class="row">
            <div class="col-md-3 col-sm-3 link-col">
              <ul class="wll-nav">
                <li class="title">Company</li>
                <li><a href="https://www.wholelattelove.com/pages/about-us">About Us</a></li>
                <li><a href="https://www.wholelattelove.com/pages/contact-us">Contact Us</a></li>
                <li><a class="compare-link disabled" href="https://www.wholelattelove.com/#">Compare-o-matic</a></li>
                <li><a href="http://www.youtube.com/subscription_center?add_user=wholelattelovetv" target="_blank">Videos</a></li>
              </ul>
            </div>

            <div class="col-md-3 col-sm-3 link-col">
              <ul class="wll-nav">
                <li class="title">Community</li>
                <li><a href="https://www.wholelattelove.com/community/categories/product-comparisons">Comparisons</a></li>
                <li><a href="https://www.wholelattelove.com/community/categories/product-reviews">Reviews</a></li>
                <li><a href="https://www.wholelattelove.com/community/categories/whole-latte-love-quick-tips">Quick Tips</a></li>
                <li><a href="https://www.wholelattelove.com/community/categories/tech-tips-expert-advice">Tech Tips</a></li>
                <li><a href="https://www.wholelattelove.com/community/categories/blog">Blog</a></li>
              </ul>
            </div>

            <div class="col-md-3 col-sm-3 link-col">
              <ul class="wll-nav">
                <li class="title">Care Center</li>
                <li><a href="https://www.wholelattelove.com/pages/latte-rewards">Latte Rewards</a></li>
                <li><a href="https://www.wholelattelove.com/pages/terms">FAQs and Policies</a></li>
                <li><a href="https://www.wholelattelove.com/pages/commercial-policies">Commercial Policies</a></li>
                <li><span data-elevio-article="51080">Coupon Exclusions</span></li>
                <li><a href="https://www.wholelattelove.com/pages/repair-center">Repair Center</a></li>
              </ul>
            </div>

            <div class="col-md-3 col-sm-3 link-col">
              <ul class="wll-nav">
                <li class="title">Get Help</li>
                <li>
                    <a class="text" href="https://www.wholelattelove.com/login">Sign In</a>
                    &nbsp;| &nbsp;<a href="https://www.wholelattelove.com/signup">Create Account</a>
                </li>
                <li><span data-elevio-article="52272">Order Questions</span></li>
                <li><span data-elevio-article="52270">Shipping &amp; Delivery</span></li>
                <li><span data-elevio-article="52271">Returns</span></li>
                <li><a href="https://wiki.wholelattelove.com/Main_Page">Support Wiki</a></li>
              </ul>
            </div>
          </div>
        </div>

          <div class="col-md-3 connect-with-us">
            <span class="title">Connect with Us!</span>
            <ul class="social-networks">
              <li><a class="icon-facebook-with-circle" href="https://www.facebook.com/WholeLatteLoveOnline/" title="facebook" target="_blank"></a></li>
              <li><a class="icon-twitter-with-circle" href="http://twitter.com/wholelattelove" title="twitter" target="_blank"></a></li>
              <li><a class="icon-youtube-with-circle" href="http://www.youtube.com/wholelattelovetv" title="youtube" target="_blank"></a></li>
              <li><a class="icon-google-with-circle" href="http://plus.google.com/+Wholelattelovepage" title="google+" target="_blank" rel="publisher"></a></li>
              <li><a class="icon-pinterest-with-circle" href="http://pinterest.com/wholelattelove/" title="pinterest" target="_blank"></a></li>
              <li><a class="icon-instagram-with-circle" href="http://instagram.com/wholelattelovedotcom/" title="instagram" target="_blank"></a></li>
            </ul>

            <span class="est">All times below are EST.</span>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="container">
          <div class="col-md-10">
            <div class="row contact">
              <div class="col-md-4">
                <div class="row">
                  <div class="col-md-12">
                    <span class="contact-title"><i class="fa fa-phone"></i> &nbsp;Sales Team</span>
                  </div>
               
                  <div class="col-md-12 hours">
                    <p>via live chat, <a href="mailto:sales@wholelattelove.com">email</a> or <a href="tel:1-888-411-5282">888-411-5282</a> (Option 1)</p>
                    
                    <div>
                      <span class="day">Monday - Thursday:</span><span>9am - 9pm</span>
                    </div>
            
                    <div>
                      <span class="day">Friday:</span><span>10am - 3pm</span>
                    </div>
                    
                    <div>
                      <span class="day">Saturday 12/31:</span><span>10am - 2pm</span>
                    </div>
                    
                    <div>
                      <span class="day">New Years Day:</span><span>CLOSED</span>
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-md-4">
                <div class="row">
                  <div class="col-md-12">
                    <span class="contact-title"><i class="fa fa-users"></i> &nbsp;Customer Service</span>
                  </div>
               
                  <div class="col-md-12 hours">
                    <p>via live chat, <a href="mailto:customerservice@wholelattelove.com">email</a> or <a href="tel:1-888-411-5282">888-411-5282</a> (Option 2)</p>
                    <div>
                      <span class="day">Monday - Thursday:</span><span>9am - 9pm</span>
                    </div>
                    
                    <div>
                      <span class="day">Friday:</span><span>10am - 3pm</span>
                    </div>
                    
                    <div>
                      <span class="day">Saturday 12/31:</span><span>10am - 2pm</span>
                    </div>
                    
                    <div>
                      <span class="day">New Years Day:</span><span>CLOSED</span>
                    </div>
                  </div>
                </div>
              </div>
              
              <div class="col-md-4">
                <div class="row">
                  <div class="col-md-12">
                    <span class="contact-title"><i class="fa fa-life-ring"></i> &nbsp;Technical Support</span>
                  </div>
                  
                  <div class="col-md-12 hours">
                    <p>via live chat, <a href="mailto:support@wholelattelove.com">email</a> or <a href="tel:1-888-411-5282">888-411-5282</a> (Option 3)</p>
                    
                    <div>
                      <span class="day">Monday - Thursday:</span><span>10am - 8pm</span>
                    </div>
                    
                     <div>
                      <span class="day">Friday:</span><span>10am - 3pm</span>
                    </div>
                    
                    <div>
                      <span class="day">Saturday 12/31:</span><span>10am - 2pm</span>
                    </div>

                    <div>
                      <span class="day">New Years Day:</span><span>CLOSED</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
                  
          <div class="col-md-2">
            <div class="row contact">
              <div class="col-md-12">
                <div class="chat-undefined" style="display: none;">
                  <i class="fa fa-comments-o"></i>
                  <span><a class="email-link" href="mailto:customerservice@wholelattelove.com">Email</a><br>or Chat</span>
                </div>
              
                <div class="chat-available" style="display: none;">
                  <a class="show-chat-box">
                    <i class="fa fa-comments-o chat-on"></i>
                    <span class="chat-on">Chat Live Now!</span><br>
                  </a>
                </div>

                <div class="chat-unavailable" style="display: block;">
                  <i class="fa fa-comments-o chat-off"></i>
                  <span class="chat-off">Chat Unavailable</span><br>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    
  

  <div class="footer-bot">
    <div class="container">
      <div class="row">
        <span>We accept the following forms of payment:&nbsp;&nbsp;</span>

        <img alt="Visa | Mastercard | Discover | Amex" src="./Espresso Machines, Coffee Makers &amp; Coffee - WholeLatteLove.com_files/payment-bbc0fce95fd9d5cdfdfbe5a288ccd4ac.png"><a href="https://www.paypal.com/webapps/mpp/paypal-popup" title="How PayPal Works" onclick="javascript:window.open(&#39;https://www.paypal.com/webapps/mpp/paypal-popup&#39;,&#39;WIPaypal&#39;,&#39;toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=1060, height=700&#39;); return false;"><img alt="PayPal" src="./Espresso Machines, Coffee Makers &amp; Coffee - WholeLatteLove.com_files/paypal-ca1076627a57d212f4258be866b57346.png"></a>

        <div itemscope="" itemtype="http://schema.org/Organization">

          <span class="copyright">©2016 <span itemprop="name">Whole Latte Love</span>. All Rights Reserved.</span>

          <meta itemprop="url" content="https://www.wholelattelove.com">
          <meta itemprop="logo" content="https://www.wholelattelove.com/assets/stores/wholelattelove/wll-logo-606e7055f06e1da9505d84cdc0fb8499.png">
          <div itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
            <meta itemprop="streetAddress" content="2200 Brighton Henrietta Townline Road">
            <meta itemprop="addressLocality" content="Rochester">
            <meta itemprop="addressRegion" content="NY">
            <meta itemprop="postalCode" content="14623">
            <meta itemprop="addressCountry" content="USA">
          </div>
        </div>
      </div>
    </div>
  </div>
</footer>

')
                     